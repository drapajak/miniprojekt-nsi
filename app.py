from datetime import datetime
import bcrypt
import json
import os
from flask import *
from flask_login import *

app = Flask(__name__)
app.secret_key = os.urandom(24)

USER_DATA_FILE = 'users.json'
DATA_FILE = 'data.json'

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

class User(UserMixin):
    def __init__(self, username):
        self.id = username

@login_manager.user_loader
def load_user(user_id):
    users = load_users()
    if user_id in users:
        return User(user_id)
    return None

def initialize_users_file():
    if not os.path.exists(USER_DATA_FILE):
        with open(USER_DATA_FILE, 'w') as f:
            json.dump({}, f)

initialize_users_file()

def load_data():
    if os.path.exists(DATA_FILE):
        with open(DATA_FILE, 'r') as f:
            return json.load(f)
    else:
        return {}

def load_users():
    if os.path.exists(USER_DATA_FILE):
        with open(USER_DATA_FILE, 'r') as f:
            return json.load(f)
    else:
        return {}

def save_users(users):
    with open(USER_DATA_FILE, 'w') as f:
        json.dump(users, f)

def redirect_dest(home):
    dest_url = request.args.get('next')
    if not dest_url:
        dest_url = url_for(home)
    return redirect(dest_url)

@app.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        users = load_users()
        if username in users:
            return jsonify({'message': 'Uživatelské jméno již existuje!'}), 400
        hashed_password = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt()).decode('utf-8')
        users[username] = hashed_password
        save_users(users)
        return jsonify({'message': 'Registrace úspěšná!'}), 200
    return render_template('register.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        users = load_users()
        if username in users and bcrypt.checkpw(password.encode('utf-8'), users[username].encode('utf-8')):
            user = User(username)
            login_user(user)
            return jsonify({'message': 'Přihlášení úspěšné!'}), 200
        else:
            return jsonify({'message': 'Nesprávné jméno nebo heslo!'}), 400
    return render_template('login.html')

@app.route('/filter-data', methods=['POST'])
@login_required
def update_items_count():
    data = request.get_json()
    items_count = int(data.get('itemsCount', 5))
    all_data = load_data()
    filtered_data = all_data[:items_count]
    return jsonify(filtered_data)

@app.route('/delete-oldest-items', methods=['POST'])
@login_required
def delete_oldest_items():
    data = request.get_json()
    count = int(data.get('count', 1))
    all_data = load_data()
    all_data.sort(key=lambda x: datetime.strptime(x.get('timestamp', '0000-00-00-000000'), '%Y-%m-%d-%H%M%S'))
    all_data = all_data[count:]
    with open(DATA_FILE, 'w') as f:
        json.dump(all_data, f)
    new_total_items = len(all_data)
    return jsonify({'message': f'Bylo smazáno {count} nejstarších položek.', 'total_items': new_total_items}), 200


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('login'))

@app.route('/', methods=['GET'])
@login_required
def index():
    data = load_data()
    total_items = len(data)
    return render_template('index.html', items=data, total_items=total_items)

if __name__ == '__main__':
    app.run(debug=True)
